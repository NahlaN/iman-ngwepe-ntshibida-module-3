import 'package:flutter/material.dart';

class Welcome extends StatefulWidget {
  const Welcome({Key? key}) : super(key: key);

  @override
  WelcomeState createState() => WelcomeState();
}

class WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/welcome.png'), fit: BoxFit.cover),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: Center(
            child: ElevatedButton(
              onPressed: () {
                Navigator.popAndPushNamed(context, '/first'); //fix routes
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.amber,
              ),
              child: const Text('Welcome', style: TextStyle(fontSize: 30)),
            ),
          ),
        ));
  }
}
