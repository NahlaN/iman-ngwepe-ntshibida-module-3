// ignore_for_file: unnecessary_const

import 'package:flutter/material.dart';
import 'closing.dart';
import 'package:flutter/src/material/icons.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.green,
            child: const Icon(Icons.arrow_forward),
            onPressed: () {
              Navigator.popAndPushNamed(context, '/last');
            },
          ),
          body: SafeArea(
              child: Column(
            children: const <Widget>[
              CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage('assets/profilepic.jpg'),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Iman Maryam Morongwa Ngwepe-Ntshibida',
                style: TextStyle(
                  fontFamily: 'Monospace',
                  fontSize: 20.0,
                  color: Colors.green,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 40.0,
              ),
              Text(
                'STUDENT',
                style: TextStyle(
                  fontFamily: 'Verdana',
                  fontSize: 18.0,
                  color: Colors.green,
                  letterSpacing: 2.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Card(
                  color: Colors.white,
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                      leading: Icon(
                        Icons.phone,
                        color: Colors.teal,
                      ),
                      title: Text('083 588 7198',
                          style: TextStyle(
                            color: Colors.green,
                            fontFamily: 'Inconsolata',
                            fontSize: 12.0,
                          )))),
              SizedBox(
                height: 30.0,
                //width: 150.0,
                child: Divider(
                  color: Colors.white,
                ),
              ),
              Card(
                  color: Colors.white,
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                      leading: Icon(
                        Icons.email,
                        color: Colors.teal,
                      ),
                      title: Text(
                        'deborah.ngwepe@gmail.com',
                        style: TextStyle(
                          color: Colors.green,
                          fontFamily: 'Inconsolata',
                          fontSize: 12.0,
                        ),
                      )))
            ],
          ))),
    );
  }
}
