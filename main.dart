import 'package:flutter/material.dart';
import 'login.dart';
import 'welcome.dart';
import 'register.dart';
import 'dashboard.dart';
import 'profile.dart';
import 'closing.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => const Welcome(),
        '/first': (context) => const Login(),
        '/second': (context) => const Register(),
        '/third': (context) => const Dashboard(),
        '/fourth': (context) => const Profile(),
        '/last': (context) => const Closing(),
        },
    );
  }
}



