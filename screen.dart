import 'package:flutter/material.dart';
import 'profile.dart';

class Closing extends StatefulWidget {
  const Closing({Key? key}) : super(key: key);
  @override
  ClosingState createState() => ClosingState();
}

class ClosingState extends State<Closing> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/helpful.png'),
              fit: BoxFit.cover),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                leading: IconButton(
                  icon: const Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.popAndPushNamed(context, '/fourth');
                  },
                )),
            body: Stack(children: [
              Container(
                  padding: const EdgeInsets.only(left: 35, top: 30),
                  child: const Text(
                    'All Done\n\nThank You',
                    style: TextStyle(color: Colors.white, fontSize: 33),
                  ))
            ])));
  }
}
